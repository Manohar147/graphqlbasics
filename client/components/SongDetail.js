import React, { Component } from "react";
import query from "../query/SongQuery";
import { graphql } from "react-apollo";
import {Link} from 'react-router';
import LyricCreate from './LyricCreate';
import LyricList from './LyricList';

class SongDetail extends Component {
  render() {
    if (this.props.data.loading) {
      return <div></div>;
    }
    const { song } = this.props.data;
    return (
      <div className="container">
        <Link to="/">Back</Link>
        <h3>{song.title}</h3>
        <LyricList lyrics={song.lyrics}/>
        <LyricCreate songId={song.id}/>
      </div>
    );
  }
}

export default graphql(query, {
  options: (props) => {
    return {
      variables: {
        id: props.params.id,
      },
    };
  },
})(SongDetail);
