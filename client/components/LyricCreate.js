import React, { Component } from "react";
import { graphql } from "react-apollo";
import gql from "graphql-tag";

class LyricCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: "",
    };
  }
  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit.bind(this)}>
          <label>Add a lyric</label>
          <input
            value={this.state.content}
            onChange={(e) => this.setState({ content: e.target.value })}
          />
        </form>
      </div>
    );
  }

  onSubmit(e) {
    e.preventDefault();
    try {
      this.props
        .mutate({
          variables: {
            songId: this.props.songId,
            content: this.state.content,
          },
        })
        .then(() => {
            this.setState({
                content: ''
            });
        });
    } catch (e) {
      console.error(e);
    }
  }
}

const mutation = gql`
mutation AddLyricToSong($content: String, $songId: ID!) {
    addLyricToSong(content: $content, songId: $songId) {
      id,
      lyrics{
        id
        content
        likes
      }
    }
  }
`;

export default graphql(mutation)(LyricCreate);
